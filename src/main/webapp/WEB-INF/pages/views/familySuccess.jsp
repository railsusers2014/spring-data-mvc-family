<%-- 
    Document   : familySuccess
    Created on : Feb 17, 2014, 1:10:35 PM
    Author     : joseph
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Family Creation Success</title>
    </head>
    <body>
        <span>${message}</span>
        <br/>
        <a href="<spring:url value="/" />">Home</a> <a href="#" onclick="history.back()">Back</a>
    </body>
</html>
