<%-- 
    Document   : familyCreate
    Created on : Feb 17, 2014, 12:23:31 PM
    Author     : joseph
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create  New Family Page</title>
    </head>
    <body>
        <h1>Create New Family</h1>
         <form:form action="processform" commandName="familyForm" class="form form-horizontal">
         <c:if test="${not empty message}"></c:if>
            <form:errors path="*" style="font-size:11px;color:red;" />
         <table border="0">
      
                 <tr>
                     <td>Name:</td>
                     <td><form:input path="name" placeholder="Enter Family Name Here" /></td>
                 </tr>
                 <tr>
                     <td>Tribe:</td>
                     <td><form:input path="tribe" placeholder="Enter Tribe Name Here" /></td>
                 </tr>
                 <tr>
                     <td>Description :</td>
                     <td><form:textarea path="description" cols="25" rows="10"/> </td>
                 </tr>
                 <tr>
                     <td></td>
                     <td></td>
                 </tr>
                 <tr >
                     <td></td>
                     <td><button type="submit">submit</button><button type="reset">reset</button></td>
             
                 </tr>
   
         </table>

    </form:form>
    </body>
</html>
