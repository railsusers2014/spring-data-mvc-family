package net.djomeda.tutorials.springdatamvcfamily.repository;

import net.djomeda.tutorials.springdatamvcfamily.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author joseph
 */
public interface PersonRepository extends JpaRepository<Person, String> {
    
}
