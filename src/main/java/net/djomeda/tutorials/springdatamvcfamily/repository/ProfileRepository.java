/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.djomeda.tutorials.springdatamvcfamily.repository;

import net.djomeda.tutorials.springdatamvcfamily.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author joseph
 */
public interface ProfileRepository extends JpaRepository<Profile, Integer>{
    
}
