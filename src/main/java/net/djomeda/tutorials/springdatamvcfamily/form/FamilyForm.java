
package net.djomeda.tutorials.springdatamvcfamily.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author joseph
 */
public class FamilyForm {
    
    
    private String ID;
   
    @NotEmpty(message = "Sorry Dude, Name can't be empty")
    @Size(min = 2, max= 20, message = "Sorry Bruv, Name length should be between 2 and 20 characters")
    private String name;
    
    @NotEmpty(message = "Tribe can't be empty, naah mate, you can't")
    @Size(min = 2, max=20,message = "Common mhenn! Tribe length should be between 2 and 20 characters")
    private String tribe;
    
    private String description;

    public FamilyForm(String name, String tribe, String description) {
        this.name = name;
        this.tribe = tribe;
        this.description = description;
    }

    public FamilyForm(String ID, String name, String tribe, String description) {
        this.ID = ID;
        this.name = name;
        this.tribe = tribe;
        this.description = description;
    }

    public FamilyForm(String name, String tribe) {
        this.name = name;
        this.tribe = tribe;
    }

    public FamilyForm() {
    }

    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the tribe
     */
    public String getTribe() {
        return tribe;
    }

    /**
     * @param tribe the tribe to set
     */
    public void setTribe(String tribe) {
        this.tribe = tribe;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }
    
    
}
