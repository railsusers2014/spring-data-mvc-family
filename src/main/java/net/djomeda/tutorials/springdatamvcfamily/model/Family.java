package net.djomeda.tutorials.springdatamvcfamily.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author joseph
 */
@Entity
@Table(name = "family")
public class Family {

    public Family() {
    }

    public Family(String ID, String name, String tribe, String description) {
        this.ID = ID;
        this.name = name;
        this.tribe = tribe;
        this.description = description;
    }

    public Family(String ID, String name, String tribe) {
        this.ID = ID;
        this.name = name;
        this.tribe = tribe;
    }
    
    
    @Id
    @Column(name = "family_id")
    private String ID;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "tribe")
    private String tribe;
    
    @Column(name = "description")
    private String description;
   
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "family")
    private Set<Person> familyMembers = new HashSet<Person>();

    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the tribe
     */
    public String getTribe() {
        return tribe;
    }

    /**
     * @param tribe the tribe to set
     */
    public void setTribe(String tribe) {
        this.tribe = tribe;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the familyMembers
     */
    public Set<Person> getFamilyMembers() {
        return familyMembers;
    }

    /**
     * @param familyMembers the familyMembers to set
     */
    public void setFamilyMembers(Set<Person> familyMembers) {
        this.familyMembers = familyMembers;
    }
    
    
    
}
