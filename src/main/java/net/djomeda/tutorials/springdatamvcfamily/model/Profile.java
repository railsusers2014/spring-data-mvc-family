package net.djomeda.tutorials.springdatamvcfamily.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author joseph
 */
@Entity
@Table(name = "profile")
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;
    @Column(name = "favorite_hobby")
    private String favoriteHobby;
    
    @Column(name = "averageGradeInSchool")
    private String averageGradeInSchool; 
   
       

    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * @return the favoriteHobby
     */
    public String getFavoriteHobby() {
        return favoriteHobby;
    }

    /**
     * @param favoriteHobby the favoriteHobby to set
     */
    public void setFavoriteHobby(String favoriteHobby) {
        this.favoriteHobby = favoriteHobby;
    }

    /**
     * @return the averageGradeInSchool
     */
    public String getAverageGradeInSchool() {
        return averageGradeInSchool;
    }

    /**
     * @param averageGradeInSchool the averageGradeInSchool to set
     */
    public void setAverageGradeInSchool(String averageGradeInSchool) {
        this.averageGradeInSchool = averageGradeInSchool;
    }
            
}
