package net.djomeda.tutorials.springdatamvcfamily.controller;

import javax.persistence.NoResultException;
import javax.validation.Valid;
import net.djomeda.tutorials.springdatamvcfamily.form.FamilyForm;
import net.djomeda.tutorials.springdatamvcfamily.model.Family;
import net.djomeda.tutorials.springdatamvcfamily.service.FamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author joseph
 */
@Controller
public class FamilyController {

@Autowired
FamilyService familyService;

    
    @RequestMapping(value = {"/family","/family/list"})
    public String showList(Model model){
        model.addAttribute("families", familyService.findAll());
        return "familyList";
    }
    
    
    @RequestMapping(value = "/family/show/{familyId}")
    public String showOne(@PathVariable("familyId") String familyId, Model model){
        model.addAttribute("family", familyService.findById(familyId));
        return "familyOne";
    }
    
    @RequestMapping(value = "/family/delete/{familyId}")
    public String deleteOne(@PathVariable("familyId") String familyId, Model model){
       try{
        familyService.delete(familyId);
         model.addAttribute("msg", "jay!, we destroyed the family");
       } catch(EmptyResultDataAccessException erdae){
           model.addAttribute("msg", "ok nephew!, the data you are trying to delete does not exist");
       } catch(Exception e){
           model.addAttribute("msg","Sorry Mate, there seems to be an issue.");
       }
        return "familyDelete";
    }
    
   
    @RequestMapping(value = "/family/edit/{familyId}")
    public ModelAndView editOne(@PathVariable("familyId") String familyId){
        ModelAndView mv = new ModelAndView("familyEdit");
        try{
           Family family = familyService.findById(familyId);
           FamilyForm form = new FamilyForm(family.getID(),family.getName(), family.getTribe(), family.getDescription());
           mv.addObject("form", form);
        }catch(NoResultException nre){
           mv.addObject("msg", "The family you wanted so bad to edit is no more");
        }
        return mv;
    }
    
    @RequestMapping(value = "/family/processeditform", method = RequestMethod.POST)
    public String processEditForm(@ModelAttribute("form") @Valid FamilyForm form, BindingResult result,Model model, final RedirectAttributes redirectAttributes){
        model.addAttribute("msg", "");
        if(result.hasErrors()){
            model.addAttribute("form", form);
            return "familyEdit";
        }
        
        try{
          Family edited = new Family();
          edited.setID(form.getID());
          edited.setName(form.getName());
          edited.setTribe(form.getTribe());
          edited.setDescription(form.getDescription());
          familyService.save(edited);
          redirectAttributes.addFlashAttribute("message", "Edited Successfully");
        }catch(Exception ex){
            
            model.addAttribute("msg", "Hi! an error Occured, disturb your chakra to wake up from genjutsu ");
            model.addAttribute("form", form);
            return "familyEdit";
        }
        return "redirect:/family/success";
    }
    
    
    
    @RequestMapping(value = "/family/create")
    public String showFamilyCreateForm(Model model){
        FamilyForm form = new FamilyForm();
        model.addAttribute("familyForm", form);
        model.addAttribute("mgs","");
        return "familyCreate";
    }
    
    @RequestMapping(value = "/family/processform")
    public String processForm(@ModelAttribute("familyForm") @Valid FamilyForm form, BindingResult result,Model model, final RedirectAttributes redirectAttributes){
        model.addAttribute("msg", "");
        if(result.hasErrors()){
            model.addAttribute("familyForm", form);
            return "familyCreate";
        }
        
        try{
          familyService.create(form.getName(),form.getTribe(), form.getDescription());
          redirectAttributes.addFlashAttribute("message", "Created Successfully");
        }catch(Exception ex){
            
            model.addAttribute("msg", "Hi! an error Occured, disturb your chakra to wake up from genjutsu");
            model.addAttribute("familyForm", form);
            return "familyCreate";
        }
        return "redirect:/family/success";
    }
    
    @RequestMapping(value = "/family/success")
    public String showSuccess(Model model){
        return "familySuccess";
    }
    
}
