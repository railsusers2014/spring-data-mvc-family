/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.djomeda.tutorials.springdatamvcfamily.controller;

import net.djomeda.tutorials.springdatamvcfamily.service.FamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author joseph
 */
@Controller
public class HomeController {
    
@Autowired
FamilyService familyService;    
    /*
     * Action to handle default url. Model is not necessary at this stage
     * It's used to send data to the view
     * 
     */
    @RequestMapping(value="/")
    public String showHome(Model model){
       model.addAttribute("families", familyService.findAll());
        return "home";
    }
    
    /*
     * This method is similar to the previous one. but this time it's to show 
     * that a single action can handle multiple urls.
     */
    @RequestMapping(value = {"/home","/welcome"})
    public String showOtherHomeUrl(Model model){
        model.addAttribute("name", "kodjo-kuma Djomeda");
        return "home";
    }
    
}
